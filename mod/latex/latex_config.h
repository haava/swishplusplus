/*
**	SWISH++
**	mod/latex/latex_config.h
**
**	Copyright (C) 2002  Paul J. Lucas
**
**	This program is free software; you can redistribute it and/or modify
**	it under the terms of the GNU General Public License as published by
**	the Free Software Foundation; either version 2 of the License, or
**	(at your option) any later version.
**
**	This program is distributed in the hope that it will be useful,
**	but WITHOUT ANY WARRANTY; without even the implied warranty of
**	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**	GNU General Public License for more details.
**
**	You should have received a copy of the GNU General Public License
**	along with this program; if not, write to the Free Software
**	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifdef	MOD_latex

#ifndef	latex_config_H
#define	latex_config_H

////////// LaTeX parameters ///////////////////////////////////////////////////

int const	Command_Name_Max_Size		= 13;
//		The maximum size of a LaTeX command name, e.g.,
//		"subsubsection".

#endif	/* latex_config_H */

#endif	/* MOD_latex */
/* vim:set noet sw=8 ts=8: */
